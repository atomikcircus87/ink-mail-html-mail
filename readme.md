## How to install

Prerequisites:
* [Node.js](http://nodejs.org/) >=0.10.0
* [Gulp.js](http://gulpjs.com/) >=3.8.0 
* [Sass](http://sass-lang.com/) >=3.4.0  

Installation process:
1. Clone this repository
2. Run ```npm install``` to install dependencies

## Usage

For project development with livereload run:
```
gulp serve
```

To build project run: (Result will be in ```dist/``` folder.)
```
gulp build
```

To serve builded project:
```
gulp serve:dist
```


* Webserver with liverelaod

* Sass compilation
* CSS concating and inlining